package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;
import ru.iteco.vetoshnikov.taskmanager.dto.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.SessionRepository;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {
    private IServiceLocator serviceLocator;
    private SessionRepository sessionRepository;

    public SessionService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public User checkPassword(
            @Nullable final String userLogin,
            @Nullable final String userPassword
    ) {
        if (userLogin == null || userLogin.isEmpty()) return null;
        if (userPassword == null || userPassword.isEmpty()) return null;
        @Nullable final List<User> userList = serviceLocator.getUserService().findAll();
        for (User getUser : userList) {
            if (userLogin.equals(getUser.getLogin())) {
                @NotNull final User user = getUser;
                @Nullable final String passwordHash = HashUtil.getHash(userPassword);
                if (passwordHash == null) return null;
                if (passwordHash.equals(user.getPassword()))
                    return user;
            }
        }
        return null;
    }

    @Override
    public Session createSession(
            @Nullable final Session session
    ) {
        if (session == null) return null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            sessionRepository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            sessionRepository.createSession(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return session;
    }
}
