package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

import javax.persistence.EntityManager;
import java.util.*;

public class ProjectRepository {
    private final EntityManager entityManager;

    public ProjectRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void merge(@NotNull final Project project) {
        entityManager.merge(project);
    }

    public void remove(@NotNull final String userId, @NotNull final String projectName) {
        entityManager.createQuery("DELETE FROM Project a WHERE a.user.id =: userId", Project.class)
                .setParameter("userId", userId);
    }

    public void clear() {
        for (Project project : findAll()) {
            entityManager.remove(project);
        }
    }

    public List<Project> findAll() {
        return entityManager.createQuery("SELECT a FROM Project a", Project.class)
                .getResultList();
    }

    public List<Project> findAllByUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT a FROM Project a WHERE a.user.id =: userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    public void persist(@NotNull final Project project) {
        entityManager.persist(project);
    }

    public void removeAllByUser(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Project a WHERE a.user.id =: userId", Project.class)
                .setParameter("userId", userId);
    }

    public void load(@NotNull final List<Project>projectList) {
        for (Project project:projectList){
            entityManager.merge(project);
        }
    }

    public String getIdProject(@NotNull final String userId, @NotNull final String projectName) {
        return entityManager.createQuery("SELECT a FROM Project a WHERE a.user.id =: userId AND a.name =: projectName", Project.class)
                .setParameter("userId", userId)
                .setParameter("projectName", projectName)
                .setMaxResults(1)
                .getSingleResult().getId();
    }

    public Project findOne(@NotNull final String userId, @NotNull final String projectName) {
        return entityManager.createQuery("SELECT a FROM Project a WHERE a.user.id =: userId AND a.name =: projectName", Project.class)
                .setParameter("userId", userId)
                .setParameter("projectName", projectName)
                .setMaxResults(1)
                .getSingleResult();
    }


}
