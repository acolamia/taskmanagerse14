package ru.iteco.vetoshnikov.taskmanager.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import javax.persistence.EntityManagerFactory;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class SqlUtil {
    public static EntityManagerFactory factory() throws Exception {
        final InputStream fis = SqlUtil.class.getClassLoader().getResourceAsStream("mysql.properties");
        final Properties property = new Properties();
        property.load(fis);

        final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, property.getProperty("db.driver"));
        settings.put(org.hibernate.cfg.Environment.URL, property.getProperty("db.host"));
        settings.put(org.hibernate.cfg.Environment.USER, property.getProperty("db.login"));
        settings.put(org.hibernate.cfg.Environment.PASS, property.getProperty("db.password"));
        settings.put(org.hibernate.cfg.Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "update");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}