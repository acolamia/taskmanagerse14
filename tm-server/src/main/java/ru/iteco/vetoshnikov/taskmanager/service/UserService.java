package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class UserService extends AbstractService implements IUserService {
    private UserRepository userRepository;

    @Override
    public void createUser(@Nullable final User user) {
        if (user == null) return;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
//            User getUser = userRepository.checkUser(user.getLogin());
//            if (getUser == null) {
                userRepository.persist(user);
//            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<User> merge(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) return null;
        for (@Nullable final User user : userList) {
            if (user == null) continue;
            try {
                entityManager=entityManagerFactory.createEntityManager();
                userRepository=new UserRepository(entityManager);
                entityManager.getTransaction().begin();
                userRepository.merge(user);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                entityManager.close();
            }
        }
        return userList;
    }

    @Override
    public void remove(@Nullable final String key) {
        if (key == null || key.isEmpty()) return;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.remove(key);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public User findOne(@Nullable final String key) {
        if (key == null || key.isEmpty()) return null;
        User user = null;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            user = userRepository.findOne(key);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public @Nullable
    final List<User> findAll() {
        List<User> userList = null;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userList = userRepository.findAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return userList;
    }

    @Override
    public void load(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            List<User> userList=new ConvertEndtityAndDTOUtil().convertDTOToUserList(domain.getUserList());
            userRepository.load(userList);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public String getIdUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        String userId = null;
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userId = userRepository.getIdUser(login);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return userId;
    }

    @Override
    public void clear() {
        try {
            entityManager=entityManagerFactory.createEntityManager();
            userRepository=new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}
