package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import lombok.*;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {
    private ProjectRepository projectRepository;

    @Override
    public void createProject(@Nullable final Project project) {
        if (project == null) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.merge(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) return;
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            try {
                entityManager = entityManagerFactory.createEntityManager();
                projectRepository = new ProjectRepository(entityManager);
                entityManager.getTransaction().begin();
                projectRepository.merge(project);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                entityManager.close();
            }
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.remove(userId, projectName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.removeAllByUser(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return null;
        Project project = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            project = projectRepository.findOne(userId, projectName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public @Nullable
    final List<Project> findAll() {
        List<Project> projectList = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectList = projectRepository.findAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return projectList;
    }

    @Override
    public @Nullable
    final List<Project> findAllByUser(@Nullable final String userId) {
        @Nullable List<Project> projectList = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectList = projectRepository.findAllByUser(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return projectList;
    }

    @Override
    public void load(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clear();
            @Nullable final List<Project> projectList = new ConvertEndtityAndDTOUtil()
                    .convertDTOToProjectList(domain.getProjectList());
            projectRepository.load(projectList);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public String getIdProject(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty() || name == null || name.isEmpty()) return null;
        String projectId = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectId = projectRepository.getIdProject(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return projectId;
    }
}
