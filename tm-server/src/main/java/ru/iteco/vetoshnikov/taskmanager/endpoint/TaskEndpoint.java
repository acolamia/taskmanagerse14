package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createTaskTask(
            @WebParam(name = "taskObject") @Nullable final TaskDTO taskObject
    ) {
        Task task = new ConvertEndtityAndDTOUtil().convertDTOToTask(taskObject);
        serviceLocator.getTaskService().createTask(task);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "taskObject") @Nullable final TaskDTO taskObject
    ) {
        Task task = new ConvertEndtityAndDTOUtil().convertDTOToTask(taskObject);
        serviceLocator.getTaskService().merge(task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        serviceLocator.getTaskService().remove(userId, projectId, taskName);
    }

    @Override
    @WebMethod
    public void clearTask(
    ) {
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    public TaskDTO findOneTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        return new ConvertEndtityAndDTOUtil()
                .convertTaskToDTO(serviceLocator.getTaskService().findOne(userId, projectId, taskName));
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllTask(
    ) {
        return new ConvertEndtityAndDTOUtil()
                .convertTaskToDTOList(serviceLocator.getTaskService().findAll());
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllByProjectTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        return new ConvertEndtityAndDTOUtil()
                .convertTaskToDTOList(serviceLocator.getTaskService().findAllByProject(userId, projectId));
    }

    @Override
    @WebMethod
    public void removeAllByProjectTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getTaskService().removeAllByProject(userId, projectId);
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "domainObject") @Nullable final DomainDTO domainObject
    ) {
        serviceLocator.getTaskService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdTaskTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        return serviceLocator.getTaskService().getIdTask(userId, projectId, taskName);
    }
}
