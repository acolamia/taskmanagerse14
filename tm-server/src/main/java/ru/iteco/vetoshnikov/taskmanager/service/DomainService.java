package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IDomainService;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

@Getter
@Setter
@RequiredArgsConstructor
public final class DomainService implements IDomainService {
    private final IServiceLocator serviceLocator;

    @Override
    public void save(@Nullable DomainDTO domain) {
        domain.setUserList(new ConvertEndtityAndDTOUtil().convertUserToDTOList(serviceLocator.getUserService().findAll()));
        domain.setProjectList(new ConvertEndtityAndDTOUtil().convertProjectToDTOList(serviceLocator.getProjectService().findAll()));
        domain.setTaskList(new ConvertEndtityAndDTOUtil().convertTaskToDTOList(serviceLocator.getTaskService().findAll()));
    }

    @Override
    public void load(@Nullable DomainDTO domain)  {
        if (domain == null) return;
        serviceLocator.getUserService().load(domain);
        serviceLocator.getProjectService().load(domain);
        serviceLocator.getTaskService().load(domain);
    }
}

