package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import ru.iteco.vetoshnikov.taskmanager.util.SqlUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Getter
@Setter
public class AbstractService {
    EntityManagerFactory entityManagerFactory;

    @SneakyThrows
    public AbstractService() {
        entityManagerFactory = SqlUtil.factory();
    }

    EntityManager entityManager;
}