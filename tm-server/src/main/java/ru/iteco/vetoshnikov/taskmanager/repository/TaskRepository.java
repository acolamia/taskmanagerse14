package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;

import javax.persistence.EntityManager;
import java.util.*;

public class TaskRepository {
    private final EntityManager entityManager;

    public TaskRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void merge(@NotNull final Task task) {
        entityManager.merge(task);
    }

    public void remove(@Nullable final String userId, String projectId, String taskName) {
        entityManager.remove(findOne(userId,projectId,taskName));
    }

    public void clear() {
        for (Task task:findAll()){
            entityManager.remove(task);
        }
    }

    public Task findOne(@NotNull final String userId, String projectId, String taskName) {
        return entityManager.createQuery("SELECT a FROM Task a WHERE a.user.id=:userId AND a.project.id=projectId AND a.name=:taskName", Task.class)
                .setParameter("userId",userId)
                .setParameter("projectId",projectId)
                .setParameter("taskName",taskName)
                .getSingleResult();
    }

    public List<Task> findAll() {
        return entityManager.createQuery("SELECT a FROM Task a").getResultList();
    }

    public List<Task> findAllByProject(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT a FROM Task a WHERE a.user.id =: userId AND a.project.id =: projectId",Task.class)
                .setParameter("userId",userId)
                .setParameter("projectId",projectId)
                .getResultList();
    }

    public void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    public void removeAllByProject(@Nullable final String userId, String projectId) {
        entityManager.createQuery("DELETE FROM Task a WHERE a.user.id =: userId AND a.project.id =: projectId", Task.class)
                .setParameter("userId",userId)
                .setParameter("projectId", projectId);
    }

    public void load(@NotNull final List<Task>taskList) {
        for (Task task:taskList){
            entityManager.merge(task);
        }
    }

    public String getIdTask(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) {
        return entityManager.createQuery("SELECT a FROM Task a WHERE a.user.id =: userId AND a.project.id =: projectId AND a.name =: name",Task.class)
                .setParameter("userId",userId)
                .setParameter("projectId",projectId)
                .setParameter("name",name)
                .setMaxResults(1)
                .getSingleResult()
                .getId();
    }
}
