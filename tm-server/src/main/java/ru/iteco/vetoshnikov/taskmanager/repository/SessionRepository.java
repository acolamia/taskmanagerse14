package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

import javax.persistence.EntityManager;

public class SessionRepository {
    private final EntityManager entityManager;

    public SessionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void createSession(@NotNull final Session session){
        entityManager.persist(session);
    }
}
