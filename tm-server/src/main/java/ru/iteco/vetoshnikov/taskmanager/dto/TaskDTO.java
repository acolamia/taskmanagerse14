package ru.iteco.vetoshnikov.taskmanager.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDTO extends AbstractDTO {
    @NotNull
    private String statusType=StatusType.PLANNED.getDisplayName();
    @Nullable
    private String projectId=null;
    @Nullable
    private String userId=null;
}