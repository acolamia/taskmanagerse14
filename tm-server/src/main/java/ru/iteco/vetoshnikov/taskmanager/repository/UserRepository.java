package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository {
    private final EntityManager entityManager;

    public UserRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void merge(@NotNull final User user) {
        entityManager.merge(user);
    }

    public void remove(@NotNull final String key) {
        entityManager.remove(entityManager.find(User.class, key));
    }

    public void clear() {
        for (User user : findAll()) {
            entityManager.remove(user);
        }
    }

    public User findOne(@NotNull final String key) {
        return entityManager.createQuery("SELECT a FROM User a WHERE a.name =: name",User.class)
                .setParameter("name",key)
                .getSingleResult();
    }

    public List<User> findAll() {
        return entityManager.createQuery("SELECT a FROM User a", User.class).getResultList();
    }

    public void persist(@NotNull final User user) {
        entityManager.persist(user);
    }

    public void load(@NotNull final List<User> userList) {
        for (User user:userList){
            entityManager.merge(user);
        }
    }

    public String getIdUser(@NotNull final String login) {
        return entityManager.createQuery("SELECT a FROM User a WHERE a.login =: login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult()
                .getId();
    }

    public User checkUser(@NotNull final String login) {
        User user= entityManager.createQuery("SELECT a FROM User a WHERE a.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult();
        if (user == null) return null;
//        if (user.getLogin()==null||user.getLogin().isEmpty())return null;
        return user;
    }
}
