package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {
    private TaskRepository taskRepository;

    @Override
    public void createTask(@Nullable final Task task) {
        if (task == null || task.getProject().getId() == null || task.getProject().getId().isEmpty()) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.merge(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Task> merge(@Nullable final List<Task> taskList) {
        if (taskList == null || taskList.isEmpty()) return null;
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            try {
                entityManager = entityManagerFactory.createEntityManager();
                taskRepository = new TaskRepository(entityManager);
                entityManager.getTransaction().begin();
                taskRepository.merge(task);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                entityManager.close();
            }
        }
        return taskList;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.remove(userId, projectId, taskName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByProject(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeAllByProject(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        Task task = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            task = taskRepository.findOne(userId, projectId, taskName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @Nullable
    final List<Task> findAll() {
        List<Task> taskList = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskList = taskRepository.findAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return taskList;
    }

    @Override
    public @Nullable
    final List<Task> findAllByProject(String userId, @Nullable final String projectId) {
        List<Task> taskList = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskList = taskRepository.findAllByProject(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return taskList;
    }

    @Override
    public void load(@Nullable final DomainDTO domain) {
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            @Nullable final List<Task> taskList = new ConvertEndtityAndDTOUtil()
                    .convertDTOToTaskList(domain.getTaskList());
            taskRepository.load(taskList);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public String getIdTask(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        String taskId = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskId = taskRepository.getIdTask(userId, projectId, taskName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return taskId;
    }
}