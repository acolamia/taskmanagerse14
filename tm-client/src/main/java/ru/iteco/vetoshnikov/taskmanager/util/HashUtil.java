package ru.iteco.vetoshnikov.taskmanager.util;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public final class HashUtil {
    public static String getHash(@NotNull final String password) {
        String passwordHash = null;
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            final StringBuilder sb = new StringBuilder();
            for (final byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            passwordHash = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return passwordHash;
    }
}
