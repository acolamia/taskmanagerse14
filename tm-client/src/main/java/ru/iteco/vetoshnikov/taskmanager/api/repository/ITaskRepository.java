package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Domain;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;

public interface ITaskRepository {
    void persist(@NotNull final Task task);

    void removeAllByProject(@NotNull final String key);

    void load(@NotNull final Domain domain);

    String getIdTask(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name);
}
