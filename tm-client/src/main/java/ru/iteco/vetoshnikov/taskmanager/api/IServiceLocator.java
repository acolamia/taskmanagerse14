package ru.iteco.vetoshnikov.taskmanager.api;


import ru.iteco.vetoshnikov.taskmanager.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.service.CommandService;
import ru.iteco.vetoshnikov.taskmanager.service.SessionStatusService;

public interface IServiceLocator {

    SessionStatusService getSessionStatusService();

    ProjectEndpointService getProjectEndpointService();

    TaskEndpointService getTaskEndpointService();

    UserEndpointService getUserEndpointService();

    DomainEndpointService getDomainEndpointService();

    SessionEndpointService getSessionEndpointService();

    CommandService getCommandService();
}
