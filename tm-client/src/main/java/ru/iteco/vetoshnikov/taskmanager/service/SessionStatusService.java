package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;

@Getter
@Setter
@NoArgsConstructor
public class SessionStatusService implements ISessionService {
    @Nullable
    private Session session = null;
}
